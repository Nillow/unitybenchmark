using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDFPS : MonoBehaviour
{
	private float accum   = 0f; // FPS accumulated over the interval
	private int   frames  = 0; // Frames drawn over the interval
	public  float frequency = 0.5F; // The update frequency of the fps

	private Text fpsText;

	void Start()
	{
		fpsText = GetComponent<Text>();
		StartCoroutine( FPS() );
	}
	
	void Update()
	{
		accum += Time.timeScale/ Time.deltaTime;
		++frames;
	}
	
	IEnumerator FPS()
	{
		// Infinite loop executed every "frenquency" secondes.
		while( true )
		{
			// Update the FPS
			float fps = accum/frames;

			if(fpsText != null)
				fpsText.text = fps.ToString("F2") + "fps";

			accum = 0.0F;
			frames = 0;
			
			yield return new WaitForSeconds( frequency );
		}
	}
}