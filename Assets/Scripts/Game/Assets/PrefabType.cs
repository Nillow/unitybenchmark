using System;

namespace Scripts.Game.Assets
{
	public enum PrefabType
	{
		NULL,
		TEST
	};

	public class PrefabVO
	{
		public PrefabType type { get; private set; }
		public String path { get; private set; }
		public int poolSize { get; private set; }
		public bool all { get; private set; }

		public PrefabVO (PrefabType type, String path, int poolSize) : this(type, path, poolSize, false) 
		{ 
		}

		public PrefabVO(PrefabType type, String path, int poolSize, bool all)
		{
			this.type = type;
			this.path = path;
			this.poolSize = poolSize;
			this.all = all;
		}
	}
}