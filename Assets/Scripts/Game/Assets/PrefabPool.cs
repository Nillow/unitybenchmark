using System;
using UnityEngine;
using System.Collections.Generic;

namespace Scripts.Game.Assets
{
	public class PrefabPool
	{
		private PrefabLoader _loader;

		public PrefabPool (PrefabLoader loader)
		{
			_loader = loader;
		}

		public void createPool()
		{
			foreach(var prefabs in _loader.getLoadedPrefabs())
			{
				if(prefabs.Value != null)
				{
					int poolSize = _loader.getPrefabVO(prefabs.Key).poolSize;
					foreach(GameObject prefab in prefabs.Value)
					{
						ObjectPool.CreatePool (prefab, poolSize);
					}
				}
			}
		}


		public void disposePool()
		{

		}
	}
}

