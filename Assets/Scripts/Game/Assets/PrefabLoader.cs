using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Scripts.Game.Assets
{
	public class PrefabLoader
	{
		private Dictionary<PrefabType, PrefabVO> registeredPrefabsVO;
		private Dictionary<PrefabType, GameObject[]> loadedPrefabs;

		public PrefabLoader ()
		{
			registeredPrefabsVO = new Dictionary<PrefabType, PrefabVO>();
			loadedPrefabs = new Dictionary<PrefabType, GameObject[]>();
		}

		public void registerPrefab(PrefabVO prefabVO)
		{
			if(!registeredPrefabsVO.ContainsKey(prefabVO.type))
			{
				registeredPrefabsVO[prefabVO.type] = prefabVO;
			}
		}

		public void load()
		{
			GameObject prefab;
			GameObject[] prefabs;
			foreach(PrefabVO prefabVO in registeredPrefabsVO.Values)
			{
				if(prefabVO.all)
				{
					prefabs = Resources.LoadAll<GameObject>(prefabVO.path);
				}
				else
				{
					prefab = Resources.Load<GameObject>(prefabVO.path);
					prefabs = new GameObject[]{prefab};
				}
				loadedPrefabs.Add(prefabVO.type, prefabs);
			}
		}

		public Dictionary<PrefabType, GameObject[]> getLoadedPrefabs()
		{
			return loadedPrefabs;
		}

		public PrefabVO getPrefabVO(PrefabType type)
		{
			if(registeredPrefabsVO.ContainsKey(type))
			{
				return registeredPrefabsVO[type];
			}
			return null;
		}

		public GameObject[] getPrefabs(PrefabType type)
		{
			if(loadedPrefabs.ContainsKey(type))
			{
				return loadedPrefabs[type];
			}
			return null;
		}

		public GameObject getPrefab(PrefabType type)
		{
			return getPrefab (type, 0);
		}

		public GameObject getPrefab(PrefabType type, int index)
		{
			if(loadedPrefabs.ContainsKey(type))
			{
				return loadedPrefabs[type][index];
			}
			return null;
		}

		public Func<GameObject> getLazyPrefab(PrefabType type)
		{
			return getLazyPrefab (type, false);
		}

		public Func<GameObject> getLazyPrefab(PrefabType type, bool random)
		{
			Func<GameObject> getPrefab = delegate() 
			{
				if(loadedPrefabs.ContainsKey(type) && loadedPrefabs[type].Length > 0)
				{
					int index = 0;
					if(random)
					{
						index = UnityEngine.Random.Range(0, loadedPrefabs[type].Length);
					}
					return loadedPrefabs[type][index];
				}
				return null;
			};

			return getPrefab;
		}
	}
}

