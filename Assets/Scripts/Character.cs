﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{

    private Vector2 _velocity;

    private Vector3 _cameraSize;

    private Rigidbody2D _rigidbody2D;

    public void SetCameraSize(Vector3 value)
    {
        _cameraSize = value;
    }

    public void SetVelocity(Vector2 value)
    {
        _velocity = value;
        _rigidbody2D.velocity = _velocity;
    }

    void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

	// Use this for initialization
	void Start ()
	{
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (transform.position.x > 2 * _cameraSize.x || transform.position.x < -2 * _cameraSize.x
	        || transform.position.y > 2 * _cameraSize.y || transform.position.y < -2 * _cameraSize.y)
	    {
	        _velocity *= -1;
            _rigidbody2D.velocity = _velocity;
        }
        //transform.Translate(_velocity);	    
    }
}
