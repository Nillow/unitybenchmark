﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Extensions;
using Scripts.Game.Assets;
using UnityEngine.UI;

public class Benchmark : MonoBehaviour
{

    public Text Count;

    private List<GameObject> items = new List<GameObject>();
    private PrefabLoader _prefabLoader;
    private PrefabPool _prefabPool;
    private Camera _camera;
    private Vector3 _cameraSize;

    // Use this for initialization
	void Start () {
        _prefabLoader = new PrefabLoader();
		_prefabLoader.registerPrefab(new PrefabVO(PrefabType.TEST, "testSwfAnim", 500));
        _prefabLoader.load();

        _prefabPool = new PrefabPool(_prefabLoader);
        _prefabPool.createPool();

        _camera = Camera.main;
        _cameraSize = _camera.OrthographicBounds().size;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 50, 50), "more"))
        {
            Vector2 position = new Vector2(Random.Range(-_cameraSize.x, _cameraSize.x), Random.Range(-_cameraSize.y, _cameraSize.y));
			Vector2 velocity = new Vector2(Random.Range(0.1f, 1f), Random.Range(0.1f, 1f));
            GameObject character = _prefabLoader.getPrefab(PrefabType.TEST).Spawn(transform, position);
            character.SendMessage("SetCameraSize", _cameraSize);
			character.SendMessage("SetVelocity", velocity);
            items.Add(character);
            UpdateCount(items.Count);
        }

        if (GUI.Button(new Rect(10, 70, 50, 30), "less"))
        {
            if (items.Count > 0)
            {
                var item = items[0];
                item.Recycle();
                items.RemoveAt(0);
            }

            UpdateCount(items.Count);
        }
    }

    void UpdateCount(int value)
    {
        Count.text = value.ToString();
    }
}
